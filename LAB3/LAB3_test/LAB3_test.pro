QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_lab3_test.cpp

SOURCES += $$files(../*.cpp)

SOURCES -= ../main.cpp

HEADERS += \
    lab3_test.h

HEADERS += ../*.h
